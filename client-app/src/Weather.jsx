import React, { useEffect, useState } from 'react';

import axios from 'axios';

function Weather() {

  const [forecasts, setAppState] = useState([]);

  useEffect(() => {
    const apiUrl = 'https://localhost:44379/WeatherForecast';
    axios.get(apiUrl).then((resp) => {
      const forecasts = resp.data;
      setAppState(forecasts);
    });
  }, [setAppState]);

  return (
    <div>
      <h1 id="tabelLabel" >Weather forecast</h1>
      <p>This component demonstrates fetching data from the server.</p>
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Date</th>
            <th>Temp. (C)</th>
            <th>Temp. (F)</th>
            <th>Summary</th>
          </tr>
        </thead>
        <tbody>
          {forecasts.map(forecast =>
            <tr key={forecast.date}>
              <td>{forecast.date}</td>
              <td>{forecast.temperatureC}</td>
              <td>{forecast.temperatureF}</td>
              <td>{forecast.summary}</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
}

export default Weather;